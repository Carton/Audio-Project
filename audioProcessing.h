/*
 * File     : audioProcessing.h
 * Author   : Pierre BRESSY
 * Modif	: Jo�l Carron
 * Company  : HEIG-VD
 * Created  : Thu Jan 31 11:04:44 2013
 * Modified : 11.02.2018
*/

#ifndef AUDIOPROCESSING_H_
#define AUDIOPROCESSING_H_

#include "dsp_types.h"
#include <std.h>
#include <stdint.h>
#include <stdbool.h>
#include <que.h>

// Preprocessor symbols
#define SAMPLING_RATE   44100     					// sampling rate in Hz

#if SAMPLING_RATE<8000
#error "SAMPLING RATE TOO LOW"
#endif

#define NUMSAMPLES	 	4800     					// number of single samples in TX or RX buffer
#define NB_CHANNELS     2     						// stereo in buffer
#define NB_LR_SAMPLES	(NUMSAMPLES / NB_CHANNELS)	// number of LRSamples

#define MAX_DELAY 2 // seconds
#define BUFFER_SIZE (MAX_DELAY*SAMPLING_RATE) // single samples (Left or Right)
#define INCREMENT_DELAY_STEP 2400
#define GAIN 2 // Possible values : integers [0,16] / Resulting gain : 1/(2^x)

#define DEFAULT_WRITE_INDEX 0
#define DEFAULT_READ_OFFSET (BUFFER_SIZE-1)

#define OVER_INT16 32768
#define OVER_UINT16 65536


// new types
typedef enum { Delay, Reverb } funcMode_t;

// enumerations

// structures
typedef struct LRSample
{
	int16 left, right;
} LRSample;

typedef struct MsgObj {
    QUE_Elem elem;
    LRSample lrSample;
} MsgObj, *Msg;

extern QUE_Obj circularQueue;
extern QUE_Obj secondCircularQueue;

// function's prototypes
void initBuffer();
void toggleFuncMode();
funcMode_t getFuncMode();
int16 addChannels(int16 *, int16 *);
void moveWriteIndex();
void decrementDelay();
void incrementDelay();
LRSample readBuffer();
void writeBuffer(LRSample *);
void audioProcess(int16 *, int16 *, uint32);

#endif /* AUDIOPROCESSING_H_ */
