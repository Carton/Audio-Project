/*
 * File     : audioProcessing.c
 * Author   : Pierre BRESSY
 * Modif	: Jo�l Carron
 * Company  : HEIG-VD
 * Created  : Thu Jan 31 11:04:44 2013
 * Modified : 11.02.2018
*/

#include "audioProcessing.h"
#include <mem.h>

funcMode_t funcMode = Delay; // functioning mode

LRSample circularBuffer[BUFFER_SIZE]; // circular buffer where to put LRSamples for further processing
uint32 writeIndex = DEFAULT_WRITE_INDEX; // in LRSample
uint32 readIndex = DEFAULT_READ_OFFSET; // distance in LRSample from current writeIndex on circularBuffer

uint32 numLRSamples = NB_LR_SAMPLES; // number of LRSample in rx-/txBuf

QUE_Obj *actualWriteQueue = &circularQueue;
QUE_Obj *actualReadQueue = &circularQueue;

LRSample yN, yNm1;

/*
 * Fill the buffer with zero values
 */
void initBuffer()
{
	int i;
	LRSample nullLRSample = {0,0};

	for (i = 0; i < BUFFER_SIZE; i++)
	{
		circularBuffer[i] = nullLRSample;
	}

	yN.left = 0;
	yN.right = 0;
	yNm1.left = 0;
	yNm1.right = 0;
}

void initQueue()
{
	Msg msg;
	uint32 i;

	for (i = 0; i < BUFFER_SIZE; i++)
	{
		msg = MEM_alloc(0, sizeof(MsgObj), 0);
		msg->lrSample.left = 0;
		msg->lrSample.right = 0;
		QUE_put(actualWriteQueue, msg);
	}
}

/*
 * Inverse the functioning mode
 */
void toggleFuncMode()
{
	funcMode = funcMode == Reverb ? Delay : Reverb;
}

/*
 * Return the actual functioning mode
 */
funcMode_t getFuncMode()
{
	return funcMode;
}

/*
 * https://dsp.stackexchange.com/a/3603
 * Function inspired by https://www.vttoth.com/CMS/index.php/technical-notes/68
 * @param firstChannel left or right channel
 * @param secondChannel left of right channel
 * @return addition of the two channels without any risk of clipping/overflow effects
 */
int16 addChannels(int16 *firstChannel, int16 *secondChannel)
{
	uint16 result = 0;
	uint16 uFirstChannel = *firstChannel + OVER_INT16;
	uint16 uSecondChannel = *secondChannel + OVER_INT16;

	if (uFirstChannel < OVER_INT16 && uSecondChannel < OVER_INT16)
	{
		result = uFirstChannel * uSecondChannel / OVER_INT16;
	}
	else
	{
	    result = (2 * (uFirstChannel + uSecondChannel)) - (uFirstChannel * uSecondChannel / OVER_INT16) - OVER_UINT16;
	}

	if (result == OVER_UINT16)
	{
		result -= 1;
	}

	return ((int16)(result -= OVER_INT16));
}


/*
 * Decrement the delay (distance between the write index at writeIndex and the read index at writeIndex+readIndex)
 */
void decrementDelay()
{
	uint32 i;
	Msg msg;

	for (i = 0; i < INCREMENT_DELAY_STEP; i++)
	{
		if (QUE_empty(actualWriteQueue)) break;

		msg = QUE_get(actualWriteQueue);
		MEM_free(0, msg, sizeof(MsgObj));
	}
}

/*
 * Increment the delay (distance between the write index at writeIndex and the read index at readIndex
 */
void incrementDelay()
{
	readIndex = readIndex >= INCREMENT_DELAY_STEP ? (readIndex - INCREMENT_DELAY_STEP) : 0;
}

/*
 * Return the content of the circular buffer at (writeIndex + readIndex) index (ie. delayed)
 */
LRSample readBuffer()
{
    return circularBuffer[(writeIndex + readIndex) % BUFFER_SIZE];
}

/*
 * Write a new LRSample into the circular buffer at writeIndex
 * Before writing, the sample is right-shifted bitwise
 * If the functioning mode is 'Reverb', we inject an old delayed sample in the buffer to create the effect
 * @param lrSample pointer on a lrSample
 */
void writeQueue(LRSample *lrSample)
{
	lrSample->left >>= GAIN;
	lrSample->right >>= GAIN;

	Msg msg;
	msg = MEM_alloc(0, sizeof(MsgObj), 0);

	if (funcMode == Reverb)
	{
		Msg reverbElem = QUE_head(actualWriteQueue);

		int16 reverbValue = reverbElem->lrSample.left >> GAIN;
		lrSample->left = addChannels(&lrSample->left, &reverbValue);

		reverbValue = reverbElem->lrSample.right >> GAIN;
		lrSample->right = addChannels(&lrSample->right, &reverbValue);
	}

	msg->lrSample.left = lrSample->left;
	msg->lrSample.right = lrSample->right;

	QUE_put(actualWriteQueue, msg);
}

LRSample readQueue(LRSample *rxLrSample)
{
	Msg msg = QUE_get(actualReadQueue);

	LRSample lrSample;

	lrSample.left = addChannels(&msg->lrSample.left, &rxLrSample->left);
	lrSample.right = addChannels(&msg->lrSample.right, &rxLrSample->right);

	MEM_free(0, msg, sizeof(MsgObj));

	return lrSample;
}

LRSample readIncrementingDelay(LRSample *rxLrSample)
{
	Msg msg = QUE_get(actualReadQueue);

	LRSample lrSample;

	lrSample.left = addChannels(&msg->lrSample.left, &rxLrSample->left);
	lrSample.right = addChannels(&msg->lrSample.right, &rxLrSample->right);

	MEM_free(0, msg, sizeof(MsgObj));

	return lrSample;
}

/*
 * rxBuf/txBuf management routine
 * Write the rxBuf in the circular buffer
 * Read the circular buffer and put this + rxBuf in the txBuf
 * @param rxBuf pointer on input buffer
 * @param txBuf pointer on output buffer
 * @param numSamples length in LRSample of rxBuf/txBuf
 */
/*
void audioProcess(int16 *rxBuf, int16 *txBuf, uint32 numLRSamples)
{
	LRSample tempRead, tempWrite;
	uint32 i;

	for (i = 0; i < numLRSamples; i += NB_CHANNELS)
	{
		memcpy(&tempWrite, &rxBuf[i], sizeof(LRSample));

		tempRead = readBuffer();
		tempRead.left = addChannels(&tempRead.left, &tempWrite.left);
		tempRead.right = addChannels(&tempRead.right, &tempWrite.right);

		memcpy(&txBuf[i], &tempRead, sizeof(LRSample));

		writeBuffer(&tempWrite);
	}
} */

void audioProcess(int16 *rxBuf, int16 *txBuf, uint32 numLRSamples)
{
	uint32 i;
	LRSample tempLrSample, tempLrSample2;

	double alpha = 0.8;
	double OneNmAplha = (1.0 - alpha);

	for (i = 0; i < numLRSamples; i += NB_CHANNELS)
	{
		//WRITE
		tempLrSample.left = rxBuf[i];
		tempLrSample.right = rxBuf[i+1];
		writeQueue(&tempLrSample);

		// READ
		tempLrSample2 = readQueue(&tempLrSample);

		yN.left = (yNm1.left * OneNmAplha) + (tempLrSample2.left * alpha) ;
		yN.right = (yNm1.right * OneNmAplha) + (tempLrSample2.right * alpha) ;

		txBuf[i] = yN.left;
		txBuf[i+1] = yN.right;

		yNm1 = yN;

	}
}

